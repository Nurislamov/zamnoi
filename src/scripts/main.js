// this is the main file that pulls in all other modules
// you can require() bower components too!

var bs = require("bootstrap");

var UI = {};
var App = {};

UI.Datepicker = function () {
  $('.datepicker').datepicker({
    weekStart: 1,
    language: 'ru',
    format: 'dd.mm.yyyy',
    orientation: 'top'
  });

  $('.fa-calendar').on('click', function () {
    let $parent = $(this).parent();
    $parent.find('.datepicker').datepicker('show');
  });
};

UI.DatepickerInline = function () {
  $('.datepicker-inline').datepicker({
    weekStart: 1,
    language: 'ru',
    format: 'dd.mm.yyyy'
  });
};

UI.RangeSlider = function () {
  $('.filter-slider').slider();
};

App.Init = function () {
  UI.Datepicker();
  UI.DatepickerInline();
  UI.RangeSlider();
}

$(document).ready(App.Init);
